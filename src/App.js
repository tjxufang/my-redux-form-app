import Form from "./components/Form";
import FormArray from "./components/FormArray";

function App() {
    return (
        <div className="App">
            <Form/>
            <br/>
            <FormArray/>
        </div>
    );
}

export default App;
