import React, {Component} from 'react';
import {connect} from 'react-redux'
import {Field, reduxForm} from "redux-form";

const validator = formValues => {
    const errors = {}
    if (!formValues.title) {
        errors.title = "Required field"
    }
    if (formValues.title?.length>10 || formValues.title?.length<2){
        errors.title = `Title is required between 2 and 10, now is ${formValues.title?.length}`
    }
    if (!formValues.email){
        errors.email = "Required field"
    }
    const re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/
    if (formValues.email && !re.test(formValues.email)) {
        errors.email = "Email Format Invalid"
    }
    return errors
}

class Form extends Component {
    onSubmit = (formValues) => {
        console.log(formValues)
    }

    renderField = ({input, label, type, meta: {touched, error}}) => (
        <div className="form-group row">
            <label className="col-sm-2 col-form-label">{label}</label>
            <div className="col-sm-10">
                <input {...input} placeholder={label} type={type} className="form-control"/>
                {touched && error && <strong>{error}</strong>}
            </div>
        </div>
    )

    render() {
        const {handleSubmit, pristine, reset, submitting} = this.props
        return (
            <div>
                <h2>Form 1</h2>
                <form onSubmit={handleSubmit(this.onSubmit)}>
                    <Field
                        name="title"
                        type="text"
                        component={this.renderField}
                        label="Title"
                    />
                    <Field
                        name="email"
                        type="email"
                        component={this.renderField}
                        label="Email"
                    />
                    <Field
                        name="password"
                        type="password"
                        component={this.renderField}
                        label="Password"
                    />
                    <button type="submit" disabled={submitting}>Create</button>
                    <button type="button" disabled={pristine || submitting} onClick={reset}>Reset</button>
                </form>
            </div>
        );
    }
}

const formWrapper = reduxForm({
    form: 'formIndex',
    validate: validator
})(Form);

export default connect(null, {})(formWrapper)