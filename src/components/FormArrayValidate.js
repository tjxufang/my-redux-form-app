const FormArrayValidate = (formValues) => {
    const errors = {}
    if (!formValues.songName) {
        errors.songName = "Required"
    }
    if (!formValues.artists || !formValues.artists.length) {
        errors.artists = {_error: "At least one artist"}
    } else {
        const artistsArrayErrors = []
        formValues.artists.forEach((artist, i) => {
            const artistErrors = {}
            if (!artist || !artist.firstname) {
                artistErrors.firstname = "Required"
                artistsArrayErrors[i] = artistErrors
            }
            if (!artist || !artist.lastname) {
                artistErrors.lastname = "Required"
                artistsArrayErrors[i] = artistErrors
            }
            if (artist && artist.phones && artist.phones.length) {
                const phoneArrayErrors = []
                artist.phones.forEach((phone, i) => {
                    if (!phone || !phone.length) {
                        phoneArrayErrors[i] = "Required"
                    }
                })
                if (phoneArrayErrors.length) {
                    artistErrors.phones = phoneArrayErrors
                    artistsArrayErrors[i] = artistErrors
                }
                if (artist.phones.length > 2) {
                    if (!artistErrors.phones) {
                        artistErrors.phones = []
                    }
                    artistErrors.phones = {_error:  "No more than 2 phones allowed"}
                    artistsArrayErrors[i] = artistErrors
                }
            }

        })
        if (artistsArrayErrors.length) {
            errors.artists = artistsArrayErrors
        }
    }
    return errors
}

export default FormArrayValidate;