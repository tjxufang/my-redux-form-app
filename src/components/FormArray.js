import React, {Component} from 'react';
import {Field, FieldArray, reduxForm} from "redux-form";
import {connect} from "react-redux";
import FormArrayValidate from "./FormArrayValidate";

class FormArray extends Component {
    onSubmit = (formValues) => {
        console.log(formValues)
    }

    renderField = ({input, label, type, meta: {touched, error}}) => (
        <div className="form-group row">
            <label className="col-sm-2 col-form-label">{label}</label>
            <div className="col-sm-10">
                <input {...input} placeholder={label} type={type} className="form-control"/>
                {touched && error && <strong>{error}</strong>}
            </div>
        </div>
    )
    renderFieldArray = ({fields, meta: {touched, error, submitFailed}}) => (
        <ul>
            <li>
                <button type="button" onClick={() => fields.push({})}>Add Artist</button>
                {(touched || submitFailed) && error && <strong>{error}</strong>}
            </li>
            {fields.map((artist, i) => (
                <li key={i}>
                    <h4>Artist {i + 1}</h4>
                    <Field
                        name={`${artist}.firstname`}
                        type="text"
                        component={this.renderField}
                        label="First Name"
                    />
                    <Field
                        name={`${artist}.lastname`}
                        type="text"
                        component={this.renderField}
                        label="Last Name"
                    />
                    <button type="button" title="remove artist" onClick={() => fields.remove(i)}>Delete</button>
                    <FieldArray name={`${artist}.phones`} component={this.renderPhoneField}/>
                </li>
            ))}
        </ul>
    )
    renderPhoneField = ({fields, meta: {error}}) => (
        <ul>
            <li>
                <button type="button" onClick={() => fields.push()}>Add Phone</button>
            </li>
            {fields.map((phone, i) => (
                <li key={i}>
                    <Field
                        name={phone}
                        type="text"
                        component={this.renderField}
                        label={`Phone #${i + 1}`}
                    />
                    <button type="button" title="Remove Phone" onClick={() => fields.remove(i)}>Delete</button>
                </li>
            ))}
            {error && <strong>{error}</strong>}
        </ul>
    )

    render() {
        const {handleSubmit, pristine, reset, submitting} = this.props
        return (
            <div>
                <h2>Form Array</h2>
                <form onSubmit={handleSubmit(this.onSubmit)}>
                    <Field
                        name="songName"
                        type="text"
                        component={this.renderField}
                        label="Song Name"
                    />
                    <br/>
                    <FieldArray name="artists" component={this.renderFieldArray}/>
                    <button type="submit" disabled={submitting}>Create</button>
                    <button type="button" disabled={pristine || submitting} onClick={reset}>Reset</button>
                </form>
            </div>
        );
    }
}

const formWrapper = reduxForm({
    form: 'formArray',
    validate: FormArrayValidate,
})(FormArray);

export default connect(null, {})(formWrapper)